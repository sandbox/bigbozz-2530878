(function($) {
Drupal.behaviors.myBehavior = {
  attach: function (context) {

	$('#bfcitate span').hide();
	$('#bfframe').hide();
	
	$('#bfcitate a').hover(function(e){
		e.preventDefault();
		var $this = $(this).parent().find('#bfframe');
		$("#bfframe").not($this).hide();
		$this.toggle();
	});

	$('.bfclose').hover(function(){
		$(this).parent().fadeOut();
		$(this).parent().hide();
	});	

  }
};
})(jQuery);